# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"

# from typing import Any, Text, Dict, List
#
# from rasa_sdk import Action, Tracker
# from rasa_sdk.executor import CollectingDispatcher
#
#
# class ActionHelloWorld(Action):
#
#     def name(self) -> Text:
#         return "action_hello_world"
#
#     def run(self, dispatcher: CollectingDispatcher,
#             tracker: Tracker,
#             domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
#
#         dispatcher.utter_message(text="Hello World!")
#
#         return []

from typing import Text, List, Any, Dict

from rasa_sdk import Tracker, FormValidationAction, Action
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.types import DomainDict
from rasa_sdk.events import SlotSet, AllSlotsReset

import re

class ValidateBeginnerBashQuizForm(FormValidationAction):
    def __init__(self):
        self.score = 0
        self.commandDic = {
            "Q1": "cd",
            "Q2": "mkdir",
            "Q3": "cat",
            "Q4": "ls",
            "Q5": "mv",
            "Q6": "grep",
            "Q7": "touch",
            "Q8": "rm",
            "Q9": "find",
            "Q10": "history"
        }
        self.wrong = []
        self.feedbackValues = ""


    def name(self) -> Text:
        return "validate_beginner_bash_quiz_form"

    @staticmethod
    def beginnerQ1_db() -> List[Text]:
        """Database of supported responses for beginner Q1"""

        return ["cd"]

    @staticmethod
    def beginnerQ2_db() -> List[Text]:
        """Database of supported responses for beginner Q2"""

        return ["mkdir"]

    @staticmethod
    def beginnerQ3_db() -> List[Text]:
        """Database of supported responses for beginner Q3"""

        return ["cat"]

    @staticmethod
    def beginnerQ4_db() -> List[Text]:
        """Database of supported responses for beginner Q4"""

        return ["ls"]

    @staticmethod
    def beginnerQ5_db() -> List[Text]:
        """Database of supported responses for beginner Q5"""

        return ["mv"]

    @staticmethod
    def beginnerQ6_db() -> List[Text]:
        """Database of supported responses for beginner Q6"""

        return ["grep"]

    @staticmethod
    def beginnerQ7_db() -> List[Text]:
        """Database of supported responses for beginner Q7"""

        return ["touch"]

    @staticmethod
    def beginnerQ8_db() -> List[Text]:
        """Database of supported responses for beginner Q8"""

        return ["rm -r"]

    @staticmethod
    def beginnerQ9_db() -> List[Text]:
        """Database of supported responses for beginner Q9"""

        return ["find"]

    @staticmethod
    def beginnerQ10_db() -> List[Text]:
        """Database of supported responses for beginner Q10"""
        
        return ["history"]

    def generate_feedback(self, slot):
        item = re.search(r"(Q\d{1,2})", slot)
        self.wrong.append(self.commandDic[item.group(0)])

        if len(self.wrong) > 2:
            self.feedbackValues = ', '.join(self.wrong)
        else:
            self.feedbackValues = ' and '.join(self.wrong)
        return self.feedbackValues

    def common_validation(self, slot, val, db, score):
        if val.lower() in db:
            self.score += 1

            return {slot: val, "score": self.score}
        else:
            fdbk = self.generate_feedback(slot)

            return {slot: val, "quiz_feedback": fdbk, "feedback_test": True}

    def validate_beginnerQ1(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.beginnerQ1_db()
        slot = "beginnerQ1"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update

    def validate_beginnerQ2(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.beginnerQ2_db()
        slot = "beginnerQ2"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update

    def validate_beginnerQ3(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.beginnerQ3_db()
        slot = "beginnerQ3"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update

    def validate_beginnerQ4(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.beginnerQ4_db()
        slot = "beginnerQ4"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update

    def validate_beginnerQ5(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.beginnerQ5_db()
        slot = "beginnerQ5"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update

    def validate_beginnerQ6(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.beginnerQ6_db()
        slot = "beginnerQ6"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update

    def validate_beginnerQ7(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.beginnerQ7_db()
        slot = "beginnerQ7"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update

    def validate_beginnerQ8(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.beginnerQ8_db()
        slot = "beginnerQ8"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update

    def validate_beginnerQ9(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.beginnerQ9_db()
        slot = "beginnerQ9"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update

    def validate_beginnerQ10(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.beginnerQ10_db()
        slot = "beginnerQ10"
        update = self.common_validation(slot, slot_value, db, self.score)

        self.wrong = []
        self.feedbackValues = ""

        return update


######################

class ValidateIntermediateBashQuizForm(FormValidationAction):
    def __init__(self):
        self.score = 0
        self.commandDic = {
            "Q1": "cd",
            "Q2": "mkdir",
            "Q3": "cat",
            "Q4": "ls",
            "Q5": "mv",
            "Q6": "grep",
            "Q7": "touch",
            "Q8": "cp",
            "Q9": "find",
            "Q10": "pwd"
        }
        self.wrong = []
        self.feedbackValues = ""

    def name(self) -> Text:
        return "validate_intermediate_bash_quiz_form"

    @staticmethod
    def intermediateQ1_db() -> List[Text]:
        """Database of supported responses for intermediate Q1"""

        return ["cd .."]

    @staticmethod
    def intermediateQ2_db() -> List[Text]:
        """Database of supported responses for intermediate Q2"""

        return ["mkdir /home/cats"]

    @staticmethod
    def intermediateQ3_db() -> List[Text]:
        """Database of supported responses for intermediate Q3"""

        return ["cat config.yml"]

    @staticmethod
    def intermediateQ4_db() -> List[Text]:
        """Database of supported responses for intermediate Q4"""

        return ["ls -al"]

    @staticmethod
    def intermediateQ5_db() -> List[Text]:
        """Database of supported responses for intermediate Q5"""

        return ["mv /models/backup.bak /home/backups/chatbot_models", "mv /backup.bak /home/backups/chatbot_models"]

    @staticmethod
    def intermediateQ6_db() -> List[Text]:
        """Database of supported responses for intermediate Q6"""

        return ["grep '[^0-9]' users.txt", 'grep "[^0-9]" users.txt']

    @staticmethod
    def intermediateQ7_db() -> List[Text]:
        """Database of supported responses for intermediate Q7"""

        return ["touch /home/www/main.html"]

    @staticmethod
    def intermediateQ8_db() -> List[Text]:
        """Database of supported responses for intermediate Q8"""

        return ["cp init.config init_backup.config"]

    @staticmethod
    def intermediateQ9_db() -> List[Text]:
        """Database of supported responses for intermediate Q9"""

        return ["find -name '*.yml'", 'find -name "*.yml"']

    @staticmethod
    def intermediateQ10_db() -> List[Text]:
        """Database of supported responses for intermediate Q10"""

        return ["pwd"]


    def generate_feedback(self, slot):
        item = re.search(r"(Q\d{1,2})", slot)
        self.wrong.append(self.commandDic[item.group(0)])

        if len(self.wrong) > 2:
            self.feedbackValues = ', '.join(self.wrong)
        else:
            self.feedbackValues = ' and '.join(self.wrong)
        return self.feedbackValues

    def common_validation(self, slot, val, db, score):
        if val.lower() in db:
            self.score += 1
            return {slot: val, "score": self.score}
        else:
            fdbk = self.generate_feedback(slot)

            return {slot: val, "quiz_feedback": fdbk, "feedback_test": True}

    def validate_intermediateQ1(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.intermediateQ1_db()
        slot = "intermediateQ1"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update

    def validate_intermediateQ2(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.intermediateQ2_db()
        slot = "intermediateQ2"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update

    def validate_intermediateQ3(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.intermediateQ3_db()
        slot = "intermediateQ3"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update

    def validate_intermediateQ4(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.intermediateQ4_db()
        slot = "intermediateQ4"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update

    def validate_intermediateQ5(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.intermediateQ5_db()
        slot = "intermediateQ5"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update

    def validate_intermediateQ6(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.intermediateQ6_db()
        slot = "intermediateQ6"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update

    def validate_intermediateQ7(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.intermediateQ7_db()
        slot = "intermediateQ7"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update

    def validate_intermediateQ8(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.intermediateQ8_db()
        slot = "intermediateQ8"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update

    def validate_intermediateQ9(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.intermediateQ9_db()
        slot = "intermediateQ9"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update

    def validate_intermediateQ10(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.intermediateQ10_db()
        slot = "intermediateQ10"
        update = self.common_validation(slot, slot_value, db, self.score)

        self.wrong = []
        self.feedbackValues = ""

        return update




class ValidateAdvancedBashQuizForm(FormValidationAction):
    def __init__(self):
        self.score = 0
        self.commandDic = {
            "Q1": "locate",
            "Q2": "cd",
            "Q3": "ls",
            "Q4": "mv",
            "Q5": "tail",
            "Q6": "cp",
            "Q7": "vi",
            "Q8": ":wq",
            "Q9": "tar",
            "Q10": "rm"
        }
        self.wrong = []
        self.feedbackValues = ""

    def name(self) -> Text:
        return "validate_advanced_bash_quiz_form"

    @staticmethod
    def advancedQ1_db() -> List[Text]:
        """Database of supported responses for advanced Q1"""

        return ["locate product-faq.yml"]

    @staticmethod
    def advancedQ2_db() -> List[Text]:
        """Database of supported responses for advanced Q2"""

        return ["cd /home/code/projects/chatbots/data/faqs"]

    @staticmethod
    def advancedQ3_db() -> List[Text]:
        """Database of supported responses for advanced Q3"""

        return ["ls -l > file_list.txt"]

    @staticmethod
    def advancedQ4_db() -> List[Text]:
        """Database of supported responses for advanced Q4"""

        return ["mv file_list.txt /home/code/project/chatbots/faq_file_list.txt"]

    @staticmethod
    def advancedQ5_db() -> List[Text]:
        """Database of supported responses for advanced Q5"""

        return ["tail -n 20 bash-faq.yml python-data-science.yml linux.yml | grep -i 'sudo'","tail -n20 bash-faq.yml python-data-science.yml linux.yml | grep -i 'sudo'",'tail -n 20 bash-faq.yml python-data-science.yml linux.yml | grep -i "sudo"','tail -n20 bash-faq.yml python-data-science.yml linux.yml | grep -i "sudo"' ]

    @staticmethod
    def advancedQ6_db() -> List[Text]:
        """Database of supported responses for advanced Q6"""

        return ["cp config.yml{,.bak}", "cp config.yml config.yml.bak"]

    @staticmethod
    def advancedQ7_db() -> List[Text]:
        """Database of supported responses for advanced Q7"""

        return ["vi python-data-science.yml"]

    @staticmethod
    def advancedQ8_db() -> List[Text]:
        """Database of supported responses for advanced Q8"""

        return [":wq"]

    @staticmethod
    def advancedQ9_db() -> List[Text]:
        """Database of supported responses for advanced Q9"""

        return ["tar -cvzf chatbot.tar /chatbot"]

    @staticmethod
    def advancedQ10_db() -> List[Text]:
        """Database of supported responses for advanced Q10"""

        return ["rm -r"]

    def generate_feedback(self, slot):
        item = re.search(r"(Q\d{1,2})", slot)
        self.wrong.append(self.commandDic[item.group(0)])

        if len(self.wrong) > 2:
            self.feedbackValues = ', '.join(self.wrong)
        else:
            self.feedbackValues = ' and '.join(self.wrong)
        return self.feedbackValues

    def common_validation(self, slot, val, db, score):
        if val.lower() in db:
            self.score += 1
            return {slot: val, "score": self.score}
        else:
            fdbk = self.generate_feedback(slot)

            return {slot: val, "quiz_feedback": fdbk, "feedback_test": True}

    def validate_advancedQ1(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.advancedQ1_db()
        slot = "advancedQ1"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update

    def validate_advancedQ2(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.advancedQ2_db()
        slot = "advancedQ2"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update

    def validate_advancedQ3(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.advancedQ3_db()
        slot = "advancedQ3"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update

    def validate_advancedQ4(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.advancedQ4_db()
        slot = "advancedQ4"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update

    def validate_advancedQ5(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.advancedQ5_db()
        slot = "advancedQ5"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update

    def validate_advancedQ6(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.advancedQ6_db()
        slot = "advancedQ6"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update

    def validate_advancedQ7(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.advancedQ7_db()
        slot = "advancedQ7"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update

    def validate_advancedQ8(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.advancedQ8_db()
        slot = "advancedQ8"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update

    def validate_advancedQ9(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.advancedQ9_db()
        slot = "advancedQ9"
        update = self.common_validation(slot, slot_value, db, self.score)

        return update

    def validate_advancedQ10(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        db = self.advancedQ10_db()
        slot = "advancedQ10"
        update = self.common_validation(slot, slot_value, db, self.score)

        self.wrong = []
        self.feedbackValues = ""

        return update


class ResetScore(Action):
    def name(self):
        return "reset_score"

    def run(self, dispatcher, tracker, domain):
        score = tracker.get_slot('score')

        return[SlotSet("score", 0), SlotSet("quiz_feedback", ""), SlotSet("feedback_test", False), SlotSet("quiz_retake", '')]

class ResetForms(Action):
    def name(self):
        return "reset_forms"

    def run(self, dispatcher, tracker, domain):
        name = tracker.get_slot('name')
        quiz_level = tracker.get_slot('quiz_level')
        score = tracker.get_slot('score')
        quiz_feedback = tracker.get_slot('quiz_feedback')
        feedback_test = tracker.get_slot('feedback_test')
        return [AllSlotsReset(), SlotSet("name", name), SlotSet("quiz_level", quiz_level), SlotSet("score", score), SlotSet("quiz_feedback", quiz_feedback), SlotSet("feedback_test", feedback_test)]